var app = new Vue({
  el: '#app',
  data: {
    baseUrl: 'https://backend.sary.to/',
    cities:[],
    sections:[],
    purchasingAverage:[],
    formSteps:[true, false, false],
    form1step1data:{
      name: '',
      phone: '',
      city: '',
      category: '',
      purchase_average: ''
    },
    has_sales_team: true,
    registeredUserId: null,
    list_file: '',
    isMobMenuOpen: false,
      Ar_Lang: true,
      Ar: {
          homePage: 'الرئيسية',
          Features: 'المميزات',
          blog: 'المدونة',
          beSupplierBtn: 'انضم كمورّد',
          or: 'أو',
          joinCustomers: 'انضم لعملائنا',
          Lang_title: 'EN',

          buyerHomeH1: 'أفضل تجربة للشراء',
          buyerHomeH2: 'من سوق الجملة',
          buyerHomeP1: 'ساري هو منصة إلكترونية تربط أصحاب الأعمال بتجار الجملة.',
          buyerHomeP2: 'سجّل الآن واضبط توريد منشأتك بأقل تكاليف ممكنة.',
          downloadFrom: 'تحميل من',
          downloadFromAndroid: 'جوجل بلاي',
          downloadFromApple: 'أبل ستور',

          formH1: 'سجّل كمشتري',
          formH2: 'صاحب مشروع أو مدير مشتريات؟ سجّل معانا',
          formName: 'الاسم',
          formPhone: 'رقم التواصل',
          formCity: 'المدينة',
          formCategory: 'القطاع',
          formPurchaseAverage: 'المشتريات الشهرية',
          formSubmit: 'التسجيل',
          formSalesTeam: 'هل عندك فريق مشتريات؟',
          formYes: 'نعم',
          formNo: 'لا',
          formUploadList: 'ارفع قائمة مشترياتك',
          formSubmit2: 'إتمام التسجيل',
          formThank: 'شكرا لك',
          formThankMsg: 'سيتم التواصل معاك من قبل فريق المبيعات',

          featuresH1: 'انسى مشوار سوق الجملة',
          featuresB1H: 'ركز على نمو مشروعك ',
          featuresB1P: 'بدال ما تلفلف بالسوق تسعّر وتفاصل التجار، استغل وقتك صح وخل ساري يضبطك بأفضل مورد وحسبة فورية لربحيتك.',
          featuresB2H: 'قلل التكاليف خلال نموك',
          featuresB2P: 'لا تكبر مصاريفك الإدارية كل ما كبر مشروعك، ساري يسهل عليك الطلب وإدارة فواتيرك من الجوال.',
          featuresB3H: 'لا تتوهق لما يكبر مشروعك',
          featuresB3P: 'زاد الطلب و كثرت مشترياتك؟ لا تفكر تأخد مستودع وتبلش، ساري يوصل احتياجاتك خلال ثلاث ساعات.',
          
          blogAll: 'الكل',
          supplierSec1H: 'سلة تفاعلية تختار أفضل مورد وتوضح ربحيتك',
          supplierSec1P: 'عشان ينمو مشروعك وتضبط ربحيتك، ضيف منتجاتك بالسلة وحنا نقارنها بالسوق ونحسب مربحك بشكل فوري.',

          supplierSec2H: 'راجع مصاريفك أونلاين انسى قلق الفواتير الورقية',
          supplierSec2P: 'قلل التكاليف خلال نمو مشروعك بمراجعة مشترياتك أول بأول، ادخل على قائمة الطلبات وتصفح فواتيرك السابقة والحالية.          ',
          
          supplierSec3H: 'توصيل بنفس اليوم ينسيك ريحة المستودع',
          supplierSec3P: 'إذا كبر مشروعك ما ودك تخاطر بسمعتك بسبب تأخير الطلبات. اختر الوقت اللي يناسبك وطلبك بيوصلك خلال ثلاث ساعات.',

          endingSecH1: 'للحين تروح سوق الجملة؟',
          endingSecH2: 'اعتمد على ساري',

          footerSite: 'المــوقع',
          footerReg: 'التسجيل',
          footerRegSup: 'التسجيل كمورّد',
          footerRegBuy: 'التسجيل كمشتري',
          footerCopyRights:'جميع الحقوق محفوظة © 2020'
      },
      En: {
        homePage: 'Home',
        Features: 'Features',
        blog: 'Blog',
        beSupplierBtn: 'Join as a supplier',
        or: 'or',
        joinCustomers: 'join our customers',
        Lang_title: 'ع',

        buyerHomeH1: 'The best shopping experience from',
        buyerHomeH2: 'wholesale markets',
        buyerHomeP1: 'Sary is a platform that connects businesses and wholesale markets.',
        buyerHomeP2: 'Register now, and handle all procurement activities from one place, with minimum expenses',
        downloadFrom: 'Download From',
        downloadFromAndroid:'Google Play',
        downloadFromApple: 'Apple Store',

        formH1: 'Join us as a buyer',
        formH2: 'Are you a business owner or purchasing manager? Join us',
        formName: 'Name',
        formPhone: 'Mobile/telephone number',
        formCity: 'Choose your city',
        formCategory: 'Market',
        formPurchaseAverage: 'Montly purchases amount',
        formSubmit: 'Register',
        formSalesTeam: 'Do you have a sales team',
        formYes: 'Yes',
        formNo: 'No',
        formUploadList: 'Upload your purshase list',
        formSubmit2: 'Register',
        formThank: 'Thank You',
        formThankMsg: 'Our sales team will contact you soon',

        featuresH1: 'Never visit the Wholesale markets again',
        featuresB1H: 'Your business deserves attention',
        featuresB1P: 'instead of this never-ending grinding procurement process, Sary will find you the most fitting supplier with immediate price quotations.',
        featuresB2H: 'More growth, less expenses',
        featuresB2P: 'Sary gives you the privilege of managing your orders and bills at the push of your mobile buttons. No more sky-high operating expenses',
        featuresB3H: 'Never be up to your ears',
        featuresB3P: 'More demand and sales? with Sary, you get your orders within 3hrs from the comfort of your office or home .',
        
        blogAll: 'All',
        supplierSec1H: 'Profit-oriented shopping cart that picks the right supplier and shows your profitability',
        supplierSec1P: 'Our innovative cart is all for your business growth and profitability, just add all the products you want, and instantly get precise products comparisions along with your expected profits',

        supplierSec2H: 'Go electronic, and forget about paper bills',
        supplierSec2P: 'Constantly revise your bills in cordination with your business growth to minimise expenses. Access all your bills and purchases through the orders menu.',
        
        supplierSec3H: 'Same day delivery before you even know it',
        supplierSec3P: 'Do not risk your business reputation. Just pick the appropriate delivery time and get your order within 3 hours',

        endingSecH1: 'Do you still go to wholesale',
        endingSecH2: 'markets? Join Sary',

        footerSite: 'Sary',
        footerReg: 'Register',
        footerRegSup: 'Join as a new supplier',
        footerRegBuy: 'Join our family as a buyer',
        footerCopyRights:'© All Rights Reserved 2020'
    },
      lang:{},
      scroll:0,
      posts:[],
      blog_cats:[],
      isActiveCategory: '',
      something:''


  },
  created: function () {
      this.lang=this.Ar;
      window.addEventListener('scroll', this.handleScroll);
     
    },
    mounted() {
      axios.get(this.baseUrl + 'api/landing/location/city/')
      .then(response => {this.cities = response.data});

      axios.get(this.baseUrl + 'api/landing/users/category/')
      .then(response => {this.sections = response.data});

      axios.get(this.baseUrl + 'api/landing/users/purchasing-average/')
      .then(response => {this.purchasingAverage = response.data});

      axios.get('https://blog.sary.com/wp-json/wp/v2/posts')
      .then(response => {this.posts = response.data});

      axios.get('https://blog.sary.com/wp-json/wp/v2/categories/')
      .then(response => {this.blog_cats = response.data});

      
    },
    updated() {
     
  },
  methods: {
      changeLang: function () {
         
          this.Ar_Lang = !this.Ar_Lang;
          if(this.Ar_Lang){
              this.lang=this.Ar;
              document.body.style.direction = 'rtl';
              axios.get('https://blog.sary.com/wp-json/wp/v2/posts')
              .then(response => {this.posts = response.data});
              axios.get('https://blog.sary.com/wp-json/wp/v2/categories/')
              .then(response => {this.blog_cats = response.data});
          }else{
              this.lang=this.En;
              document.body.style.direction = 'ltr';
              axios.get('https://blog.sary.com/wp-json/wp/v2/posts?lang=en')
              .then(response => {this.posts = response.data});
              axios.get('https://blog.sary.com/wp-json/wp/v2/categories?lang=en')
              .then(response => {this.blog_cats = response.data});
          }
      },
      handleScroll (event) {
        // Any code to be executed when the window is scrolled
        let h = document.documentElement,
                b = document.body,
                st = 'scrollTop',
                sh = 'scrollHeight';
        this.scroll=document.documentElement[st];
        document.getElementById("screen-loader").style.width = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100 + '%';
      },
      handleFileUpload(){
        this.list_file = this.$refs.file.files[0];
        console.log(this.list_file);
      },
      submitForm1step1(event){
        event.preventDefault();
        const vm = this;
        if(this.form1step1data.name&&this.form1step1data.phone&&this.form1step1data.city&&this.form1step1data.category&&this.form1step1data.purchase_average)
        {
          //console.log("submit form 1");
          //this.formSteps=[false,true,false];
          axios.post(this.baseUrl + 'api/landing/users/crm-user/', this.form1step1data)
        .then(function (response) {
          console.log(response);
          vm.registeredUserId=response.data.id;
          console.log(response.data.id);
          vm.updateFormView(1);
          
        })
        .catch(function (error) {
          console.log(error.response);
          alert(error.response.data.phone[0] || 'error in registration, please try again')
        });
        
        }
        else{
          console.log("error required");
          
        }
      },
      submitForm1step2(event){
        event.preventDefault();
        const vm = this;
          const formdata = new FormData();
          if (this.list_file) {
            formdata.append("purchasing_file", this.list_file);
            }
          formdata.append("has_sales_team", this.has_sales_team);

        
        axios.patch(this.baseUrl + `api/landing/users/crm-user/${this.registeredUserId}/`, formdata)
        .then(function (response) {
          console.log(response);
          vm.updateFormView(2);
        })
        .catch(function (error) {
          console.log(error.response);
          alert('error in registration, please try again')
        }); 
        //event.preventDefault();
        
      },
      updateFormView(index){
        if(index===1){
          this.formSteps=[false,true,false];
          console.log(this.formSteps);
        }
        else if(index===2){
          this.formSteps=[false,false,true];
          console.log(this.formSteps);
        }
       
      },
      boomingAnimation(targetSelector, posX, posY) {
       
    },
    getAnimate(){
   if(this.scroll===600)
      return 'move';

    }
  }
});